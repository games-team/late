/* 
    This file is to parse arguments and set everything up

    Copyright, 2003 Caleb Moore scarlet@users.sourceforge.net 
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "game.h"
#include "menu.h"
#include "graphics.h"
#include "arch.h"
#include "scorescreen.h"
#include <fstream>
#include <iostream>
using namespace std; 


int cheat       = 0;
int zappy;
int hardness;
int fullscreen;

void load_prefs()
{
  char n[200];
  conffile(n);

  if (!fileexists(n)) 
    return;
  
  ifstream fin;
  fin.open(n);

  fin >> zappy;
  fin >> hardness;
  fin >> fullscreen;
  fin.close();
}

void save_prefs()
{
  char n[200];
  conffile(n);
  ofstream fout(n);

  fout << zappy << '\n';
  fout << hardness << '\n';
  fout << fullscreen << '\n';
  fout.close();
}

void optionmenu()
{
  menu optionmenu;
  optionmenu.set_title("Options");
  
  //just set up some crap options to be overwritten later
  optionmenu.add_option("Left Shoot Right Rotate",0);
  optionmenu.add_option("Easy",1);
  optionmenu.add_option("Windowed",2);
  optionmenu.add_option("Back",-1);

  while (1)
    {
      if (zappy == 0)
	optionmenu.change_option("Left Shoot Right Rotate",0,"Relitive mode\nLeft button shoots in the way the shooter is pointing");
      else if (zappy == 1) 
	optionmenu.change_option("Left Vertical Right Horizontal",0,"Absolute mode\nLeft button allways shoots horizontal right is allways vertical");
      else
	optionmenu.change_option("Left Rotate and Shoot Right Shoot",0,"Eccentric mode\nRight button will alternately fire horizonatally or vertically");

      if (hardness == 0)
	optionmenu.change_option("Easy",1, "Wimp level");
      else if (hardness == 1) 
	optionmenu.change_option("Medium",1, "Standard");
      else
	optionmenu.change_option("Hard",1, "YEAH BRING IT ON");

      if (fullscreen == 0)
	optionmenu.change_option("Windowed",2, "Hit enter to go fullscreen");
      else
	optionmenu.change_option("Fullscreen",2, "Hit enter to go to a window");
      
      
      switch(optionmenu.getchoice())
	{
	case 0:
	  zappy = (zappy + 1 ) % 3;
	  break;
	case 1:
	  hardness = (hardness + 1 ) % 3;
	  break;
	case 2:
	  fullscreen = (fullscreen + 1 ) % 2;
	  graphics_changemode();
	  break;
	case -1:
	  save_prefs();
	  return;
	}
    }
  
}

void credits()
{
  menu creditsmenu;
  creditsmenu.set_title("Credits");
  creditsmenu.add_option("Caleb Moore",0,"Programming\nArtwork\nSome other things /n /nscarlet@users.sourceforge.net");
  creditsmenu.add_option("Sean Renalds Massey Reid",1, "Additional Graphics");
  creditsmenu.add_option("Danny Robson",2, "Windows port");
  creditsmenu.add_option("Special Thanks",3, "Steve Kemp     Cheat and compilation patch \nSam Lantinga      Awesome graphics library   SDL is the source of all that is good in the world");
  creditsmenu.add_option("Back",-1);

  while (creditsmenu.getchoice() != -1)
    ;
}


int main(int argc, char** argv)
{
  
  for( int i = 1; i < argc; i++ )
    {
      if (strcmp(argv[i],"--help")== 0)
	{
	  cout << "late v" << VERSION << " by Caleb Moore, scarlet@users.sourceforge.net." << endl;
	  cout << "  Insert usage information here.\n" << endl;
	  return 0;
	}
      else if (strcmp(argv[i],"--version")== 0)
	{
	  cout << "late v" << VERSION << " by Caleb Moore, scarlet@users.sourceforge.net." << endl;
	  cout << "  See the following URL for the latest version:" << endl;
	  cout << "\thttp://late.sourceforge.net/" <<endl;
	  return 0;
	}
      else if (strcmp(argv[i],"--cheat") == 0)
	{
	  cheat = 1;
	}
      else
	{
	  cout << "Unknown argument '" << argv[i] << "'" <<endl;
	  return -1;
	}
    }

  load_prefs();
  graphics_initialize(); 
  menu mainmenu;
  mainmenu.set_title("Main Menu");
  mainmenu.add_option("Start Game",0);
  mainmenu.add_option("Options",1);
  mainmenu.add_option("Credits",2);
  mainmenu.add_option("Highscores",3);
  mainmenu.add_option("Quit",-1);

  menu exitmenu;
  exitmenu.set_title("Quit?");
  exitmenu.add_option("No",0);
  exitmenu.add_option("Yes",1);

  while (1)
    {
      switch(mainmenu.getchoice())
	{
	case 0:
	  mainloop();
	  break;
	case 1:
	  optionmenu();
	  break;
	case 2:
	  credits();
	  break;
	case 3:
	  display_highscores();
	  break;
	case -1:
	  if (exitmenu.getchoice() == 1)
	    exit(0);
	}
    }
}

