#include "goody.h"
#include "graphics.h"
#include "game.h"
using namespace std;

static int lifeupbmp;
static bool lifeupbmpassigned;

static int batterybmp;
static bool batterybmpassigned;

static int magnetbmp;
static bool magnetbmpassigned;

static int bonusbmp;
static bool bonusbmpassigned;

static int brpubmp;
static bool brpubmpassigned;

extern int lives;
extern int score;
extern int lazershots;
extern int magnetshots;
extern int bonuslevelsdue;

lifeup::lifeup(ball * pack)
{
  package = pack;
  animate = 0;
}
void lifeup::display()
{
  if (lifeupbmpassigned == 0)
    {
      lifeupbmp = load_bmpstrip("life.bmp", 20);
      lifeupbmpassigned = 1;
    }

  int pic;
  pic = lifeupbmp + int (animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * lifeup::duplicate(ball * pack)
{
  lifeup * output = new lifeup(pack);
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

int lifeup::hitchopper()
{
  lives++;
  eraseball(package->getserial()); 
  return 0;
}

void lifeup::exploded()
{
  lives++;
}

void lifeup::update(float amount)
{
  ball_base::update(amount);

  animate += 0.1 * amount;
  if (animate >= 20)
    animate = 0;
}


lazershot::lazershot(ball * pack)
{
  package = pack;
  animate = 0;
}
void lazershot::display()
{
  if (batterybmpassigned == 0)
    {
      batterybmp = load_bmpstrip("battery.bmp", 40);
      batterybmpassigned = 1;
    }

  int pic;
  pic = batterybmp + int(animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * lazershot::duplicate(ball * pack)
{
  lazershot * output = new lazershot(pack);
  output->animate = animate;
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

int lazershot::hitchopper()
{
  lazershots += 2;
  eraseball(package->getserial()); 
  return 0;
}

void lazershot::exploded()
{
  lazershots += 2;
}


void lazershot::update(float amount)
{
  ball_base::update(amount);

  animate += 0.1 * amount;
  if (animate >= 40)
    animate = 0;
}

magnetitem::magnetitem(ball * pack)
{
  package = pack;
  animate = 0;
}
void magnetitem::display()
{
  if (magnetbmpassigned == 0)
    {
      magnetbmp = load_bmpstrip("magnet.bmp", 20);
      magnetbmpassigned = 1;
    }

  int pic;
  pic = magnetbmp + int (animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * magnetitem::duplicate(ball * pack)
{
  magnetitem * output = new magnetitem(pack);
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

int magnetitem::hitchopper()
{
  magnetshots += 2;
  eraseball(package->getserial()); 
  return 0;
}

void magnetitem::exploded()
{
  magnetshots += 2;
}

void magnetitem::update(float amount)
{
  ball_base::update(amount);

  animate += 0.1 * amount;
  if (animate >= 20)
    animate = 0;
}

bonus::bonus(ball * pack)
{
  animate = 0;
  package = pack;
}
void bonus::display()
{
  if (bonusbmpassigned == 0)
    {
      bonusbmp = load_bmpstrip("bonus.bmp", 40);
      bonusbmpassigned = 1;
    }

  int pic;
  pic = bonusbmp + int (animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * bonus::duplicate(ball * pack)
{
  bonus * output = new bonus(pack);
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

int bonus::hitchopper()
{
  score += 1000;
  eraseball(package->getserial()); 
  return 0;
}

void bonus::exploded()
{
   score += 1000;
}

void bonus::update(float amount)
{
  ball_base::update(amount);

  animate += 0.1 * amount;
  if (animate >= 40)
    animate = 0;
}


brpu::brpu(ball * pack)
{
  animate = 0;
  package = pack;
}
void brpu::display()
{
  if (brpubmpassigned == 0)
    {
      brpubmp = load_bmpstrip("brpu.bmp", 41);
      brpubmpassigned = 1;
    }

  int pic;
  pic = brpubmp + int (animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * brpu::duplicate(ball * pack)
{
  brpu * output = new brpu(pack);
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

int brpu::hitchopper()
{
  bonuslevelsdue ++;
  eraseball(package->getserial()); 
  return 0;
}

void brpu::exploded()
{
   bonuslevelsdue ++;
}

void brpu::update(float amount)
{
  ball_base::update(amount);

  animate += 0.1 * amount;
  if (animate >= 40)
    animate = 0;
}

