/* 
    This is a pretty generic little bitmap text renderer

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "text.h"
#include "graphics.h"
#include <vector>
#include <iostream>
using namespace std;

class font
{
  unsigned int bitmaprange;
public:
  point size;
  font() {}
  font(char * filename);

  void render(char, unsigned int, unsigned int);
};

vector<font> fonts;

font::font(char * filename)
{
  bitmaprange = load_bmpstrip(filename,75);
  size = bmp_size(bitmaprange);
}

void font::render(char letter, unsigned int x, unsigned int y)
{
  display_bmp(bitmaprange + int(letter - '0'), x, y);
}

int load_font(char * filename)
{
  int temp = fonts.size();
  fonts.push_back(font(filename));
  return temp;
}

void print_text(string chars, int font, int x, int y)
{
  int curserx = x;
  int cursery = y; 

  for (unsigned int i = 0; i < chars.size(); i++)
    {
      if (chars[i] != ' ' && chars[i] != '\n')
	fonts[font].render(chars[i], curserx, cursery);
      if (chars[i] != '\n')
	curserx += fonts[font].size.x; 
      else
	{
	  curserx = x;
	  cursery += fonts[font].size.y;
	} 
    }
}
