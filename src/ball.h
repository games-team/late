/* 
    This file handles generic ball

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __ball_h__
#define __ball_h__

class ball;

#include "square.h"

class ball_base
{
 protected:
  floatpoint center;
  floatpoint velocity;
  square * boundary;
  ball * package;

 public:
  bool condemned;
  ball_base(); 
  ball_base(ball *);
  virtual void condemn()
    {
      condemned = 1;
    }
  virtual floatpoint getcenter()
    {
      return center;
    }
  virtual floatpoint getvelocity()
    {
      return velocity;
    }
  virtual unsigned int getradius()
    {
      return 15;
    }
  virtual square * getboundary()
    {
      return boundary;
    }
  virtual float getmass()
    {
      return 1;
    }

  virtual void setcenter(floatpoint c)
    {
      center = c;
    }
  virtual void setvelocity(floatpoint v)
    {
      velocity = v;
    }
  virtual void setboundary(square * b)
    {
      boundary = b;
    }
  virtual int hitchopper()
    {
      return 1;
    }
  virtual int ishostile()
    {
      return 1;
    }
  virtual void exploded()
    {
    }

  virtual void update(float);
  virtual bool isinside(square&);
  virtual void collide(ball *);
  virtual void specialcollide(ball *);
  virtual void specialcollideb(ball *);
  virtual void display();

  virtual ball_base * duplicate(ball *);
};

class ball
{
  unsigned int serial;
 public:
  ball_base * data;
  ball();
  ball(char);
  ball(const ball&);
  ball(ball_base *);
  ball & operator=(const ball&);

 ~ball()
    {
      if (data != NULL)
	delete data;
    }
  unsigned int getserial()
    {
      return serial;
    }
  floatpoint getcenter()
    {
      return data->getcenter();
    }
  floatpoint getvelocity()
    {
      return data->getvelocity();
    }
  unsigned int getradius()
    {
      return data->getradius();
    }
  square * getboundary()
    {
      return data->getboundary();
    }
  float getmass()
    {
      return data->getmass();
    }

  void setcenter(floatpoint c)
    {
      data->setcenter(c);
    }
  void setvelocity(floatpoint v)
    {
      data->setvelocity(v);
    }
  void setboundary(square * b)
    {
      data->setboundary(b);
    }

  void update(float amount)
    {
      data->update(amount);
    }
  bool isinside(square& around)
    {
      return data->isinside(around);
    }
  void collide(ball * other)
    {
      data->collide(other);
    }
  void specialcollide(ball * other)
    {
      data->specialcollide(other);
    }
  void specialcollideb(ball * other)
    {
      data->specialcollideb(other);
    }
  void display()
    {
      data->display();
    }
  int hitchopper()
    {
      return data->hitchopper();
    }
  int ishostile()
    {
      return data->ishostile();
    }
  void exploded()
    {
      data->exploded();
    }
};

#endif
