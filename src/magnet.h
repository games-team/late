#ifndef __magnet_h__
#define __magnet_h__

#include "point.h"
#include "square.h"

class magnet
{
 public:
  floatpoint center; 
  magnet()
    {
    };
  magnet(floatpoint);
  void update(float);
};

class magnetpair
{
  magnet magnets[2];
 public:
  float timetolive;
  magnetpair();
  magnetpair(square *, point, char, float);
  void display();
  void update(float);
};

#endif
