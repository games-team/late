/* 
    This is a flexable template for ingame menus

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include <string>
#include <iostream>
#include <SDL/SDL.h>
#include "text.h"
#include "graphics.h"
#include "menu.h"
using namespace std;

static unsigned int font;
static unsigned int littlefont;
static unsigned int background;
static unsigned int curser;
static bool assigned;

void menu::set_title(string title)
{
  heading = title;
}

void menu::add_option(string title, int key)
{
  add_option(title, key, "");
}

void menu::add_option(string title, int key, string discription)
{
  titlelist.push_back(title);
  keylist.push_back(key);
  discriptionlist.push_back(discription);
}

void menu::change_option(string title, int key)
{
  change_option(title, key, "");
}

void menu::change_option(string title, int key, string discription)
{
  for (unsigned int i = 0; i < keylist.size(); i++)
    if (keylist[i] == key)
      {
	titlelist[i] = title;
	discriptionlist[i] = discription;
	break;
      }
}

string menu::get_discription()
{
  for (unsigned int i = 0; i < keylist.size(); i++)
    if (keylist[i] == selection)
      {
	return discriptionlist[i];
      }
  return "";
}

void menu::draw()
{
  if (assigned == 0)
    {
      assigned = 1;
      font = load_font("font.bmp");
      littlefont = load_font("font2.bmp");
      background = load_bmp("menu.jpg");
      curser = load_bmp("selection.bmp"); 
    }

  display_bmp(background, 1, 1);
  display_bmp(curser, 200, 100 + selection * 20);
  
  print_text(heading, font, 300, 30);

  print_text(get_discription(), littlefont, 200, 500);

  for (unsigned int i = 0; i < titlelist.size(); i++)
    {
      print_text(titlelist[i], font, 200, 100 + i * 20);
    }
  refresh_screen();
}

int menu::getchoice()
{
  draw();
  while (1)
    {
      SDL_Event event;
      while(SDL_PollEvent(&event))
	{
	  if (event.type == SDL_MOUSEBUTTONDOWN) 
	    {
	      if (event.button.button == SDL_BUTTON_LEFT)
		{
		  if (event.button.x >= 200 && event.button.x <= 600 &&
		      event.button.y >= 100 && event.button.y < 100 + 
		      titlelist.size() * 20)
		    {
		      int temp = (event.button.y - 100) / 20;
		      if (temp == selection)
			return keylist[selection];
		      else
			selection = temp;
		      draw();
		    }
		}
	    }      
	  if (event.type == SDL_KEYDOWN) 
	    {
	      switch (event.key.keysym.sym)
		{
		case SDLK_UP:
		  selection = (selection - 1) % keylist.size();
		  draw();
		  break;
		case SDLK_DOWN:
		  selection = (selection + 1) % keylist.size();
		  draw();
		  break;
		case SDLK_RETURN:
		case SDLK_SPACE:
		  return keylist[selection];
		case SDLK_ESCAPE:
		  return -1;
		default:
		  break;
		}
	    }
	  if (event.type == SDL_QUIT) 
	    {
	      exit(0);
	    }
	}
    }
}
