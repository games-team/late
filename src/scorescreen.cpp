/* 
    This is the little summery of the players achievements in a level

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "text.h"
#include "graphics.h"
#include "game.h"
#include "highscores.h"
#include <SDL/SDL.h>
#include <sstream>
using namespace std;

static unsigned int font;
static unsigned int background;
static bool assigned;

extern int clearnessneeded;
extern int clearness;
extern int currentlevel;
extern int lives;
extern int score;

static void pause_for_input()
{
  SDL_Event event;
  while (1)
    {
      while(SDL_PollEvent(&event))
	{
	  if (event.type == SDL_MOUSEBUTTONDOWN ||
	      event.type == SDL_KEYDOWN) 
	    {
	      return;
	    }
	  
	  if (event.type == SDL_QUIT) 
	    {
	      exit(0);
	    }
	}
    }
}

void display_scorescreen()
{
  if (assigned == 0)
    {
      assigned = 1;
      background = load_bmp("lava.jpg");
      font = load_font("font.bmp");
    }

  display_bmp(background, 1, 1);
  
  char n [200];
  snprintf(n, 200, "Level: %i", currentlevel);
  print_text(n, font, 200, 10);
  snprintf(n, 200, "Lives: %i", lives);
  print_text(n, font, 200, 30);
  snprintf(n, 200, "Clearness: %i Needed: %i", clearness, clearnessneeded);
  print_text(n, font, 200, 50);

  int isol = isolation() * 100;
  int overachiever = (clearness - clearnessneeded) * 100;
  int levelbonus = currentlevel * 300;
  score = score + isol + overachiever + levelbonus;

  snprintf(n, 200, "Isolation: %i", isol);
  print_text(n, font, 200, 90);
  snprintf(n, 200, "Over Achiver: %i", overachiever);
  print_text(n, font, 200, 110);
  snprintf(n, 200, "Level Bonus: %i", levelbonus);
  print_text(n, font, 200, 130);

  snprintf(n, 200, "Score: %i", score);
  print_text(n, font, 200, 170);
  refresh_screen();
  pause_for_input();
}

void display_highscores()
{
  if (assigned == 0)
    {
      assigned = 1;
      background = load_bmp("lava.jpg");
      font = load_font("font.bmp");
    }

  vector<highscoreentry> scores;
  scores = gethighscores();

  display_bmp(background, 1, 1);
  
  print_text("Name", font, 250, 30);      
  print_text("Level", font, 530, 30);
  print_text("Score", font, 600, 30);

  for(unsigned int i = 0; i < scores.size(); i++)
    {
      string temp;
      stringstream ss;
      ss << i + 1;
      ss >> temp;
      print_text(temp, font, 200, 70 + i * 20);
      temp.clear();
      temp = scores[i].name;
      print_text(temp, font, 250, 70 + i * 20);
      temp.clear();
      ss.clear();
      ss << scores[i].level;
      ss >> temp;      
      print_text(temp, font, 550, 70 + i * 20);
      temp.clear();
      ss.clear();
      ss << scores[i].score;
      ss >> temp;     
      print_text(temp, font, 600, 70 + i * 20);
    }


  refresh_screen();
  pause_for_input();
}
