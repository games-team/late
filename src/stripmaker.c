#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdlib.h>

SDL_Surface * screen;

int main(int argc, char * argv[])
{
  if( SDL_Init(SDL_INIT_VIDEO) < 0 ) 
    {
      fprintf(stderr,
	      "Error initializing SDL: %s\n", SDL_GetError());
      exit(1);
    }
  atexit(SDL_Quit);

  SDL_Surface * strip;
  SDL_Surface * frame;
  char * filename = argv[1];
  char * filetype = argv[2];
  int start = atoi(argv[3]);
  int finish = atoi(argv[4]);

  char curname[200];


  snprintf(curname, 200, "%s%i%s", filename, start, filetype);
  frame = IMG_Load(curname);
  int framewidth = frame->w;
  
  printf("filename: %s start: %i finish: %i: framewidth %i", filename, start, finish, framewidth);

  SDL_Rect dstrect;
  dstrect.x = 0;
  dstrect.y = 0;
  SDL_Rect srcrect;
  srcrect.x = 0;
  srcrect.y = 0;
  srcrect.h = frame->h;
  srcrect.w = framewidth;

  strip = SDL_CreateRGBSurface(0, framewidth * (finish - start + 1), frame->h,
			       frame->format->BitsPerPixel, 
			       frame->format->Rmask, 
			       frame->format->Gmask,
			       frame->format->Bmask, 
			       frame->format->Amask);
  int i;
  for (i = start; i <= finish; i++)
    {
      snprintf(curname, 200, "%s%i%s", filename, i, filetype);
      frame = IMG_Load(curname);
      dstrect.x = framewidth * (i - start);
      SDL_BlitSurface(frame, &srcrect, strip, &dstrect);
    }
  snprintf(curname, 200, "%sstrip.bmp", filename, i);
  SDL_SaveBMP(strip, curname);
}
