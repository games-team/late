/* 
    This file handles the rectangular reigeons of the field of play

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __square_h__
#define __square_h__

#include "point.h"
#include <vector>
using namespace std;

class square
{
 public:
  unsigned int topy;
  unsigned int bottomy;
  unsigned int leftx;
  unsigned int rightx;

  bool isinside(const point) const;
  bool isinside(const unsigned int, const unsigned int) const;
  vector<square> chopx(const unsigned int, const unsigned int) const;
  vector<square> chopy(const unsigned int, const unsigned int) const;

  bool operator==(const square & other)
    {
      return topy == other.topy && bottomy == other.bottomy && 
	leftx == other.leftx && rightx == other.rightx;
    }
  int area()
    {
      return (bottomy - topy) * (rightx - leftx);
    }
};

#endif
