/* 
    This is a flexable template for ingame menus

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include <string>
#include <vector>
using namespace std;

class menu
{
  string heading;
  int selection;
  vector<int> keylist;
  vector<string> titlelist;
  vector<string> discriptionlist;
 public:
  menu()
    {
      selection = 0;
    }
  int getchoice();
  void set_title(string title);
  void add_option(string title, int key);
  void add_option(string title, int key, string discription);
  void change_option(string title, int key);
  void change_option(string title, int key, string discription);
  string get_discription();
  void draw();
};
