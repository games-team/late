/* 
    This is the control panel for the program, functions that when called will
    dratically changed the course of the game

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __game_h__
#define __game_h__

#include "square.h"
#include "ball.h"

void splitx(point);
void splity(point);
void reassignboundarys();
void update(float);
void display();
void mainloop();
void clearempty();
ball * iscloseto(floatpoint, float, ball *);
int coverage();
int isolation();
void restartgame();
void victorydetect();
void loadlevel(char *, int);
void ouch();
void addball(ball newball);
void eraseball(unsigned int condemned);
void blow_up(square *);
void random_place(ball * tomove);
void add_goody();


#endif
