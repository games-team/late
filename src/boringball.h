#ifndef __boringball_h__
#define __boringball_h__

#include "ball.h"

class boringball : public ball_base
{
  float animate;
  int animatestate;
 public:
  void specialcollide(ball *);
  void specialcollideb(ball *);
  boringball(ball *);
  unsigned int getradius()
    {
      return 8;
    }
  void display();
  void update(float);
  ball_base * duplicate(ball *);
};

#endif
