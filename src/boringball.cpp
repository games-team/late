#include "boringball.h"
#include "graphics.h"
#include "game.h"
using namespace std;

static unsigned int boringballbmp;
static bool boringballbmpassigned;


boringball::boringball(ball * pack)
{
  package = pack;
  animate = 0;
  animatestate = 0;
}
void boringball::display()
{
  if (boringballbmpassigned == 0)
    {
      boringballbmp = load_bmpstrip("firestrip.bmp",14);
      boringballbmpassigned = 1;
    }

  int pic;
  pic = boringballbmp + int(animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2 + 5); 
}

ball_base * boringball::duplicate(ball * pack)
{
  boringball * output = new boringball(pack);
  output->animate = animate;
  output->animatestate = animatestate;  
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

void boringball::update(float amount)
{
  ball_base::update(amount);



  if (animatestate == 0)
    {
      animate += 0.05 * amount;
      if (animate >= 14)
	animate = 4;
      return;
    }
  if (animatestate == 1)
    {
      animate -= 0.1 * amount;
      if (animate <= 3)
	{
	  animatestate = 0;
	  animate = 0;
	}
      return;
    }
  if (animatestate == 2)
    {
      animate += 0.1 * amount;
      if (animate >= 10)
	{
	  animatestate = 1;
	  animate = 9.9;
	}
      return;
    }
  if (animatestate == 3)
    {
      animate -= 0.1 * amount;
      if (animate <= 1)
	{
	  animatestate = 2;
	  animate = 1;
	}
      return;
    }
  
}

void boringball::specialcollide(ball * other)
{
  animatestate = 3;
  other->specialcollideb(package);
}

void boringball::specialcollideb(ball * other)
{
  animatestate = 3;
}


