/* 
    This file handles little points and inline functions to manipulate them

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __point_h__
#define __point_h__

#include <cmath>

struct floatpoint;

struct point
{
  point()
  {
  }
  point(const int newx, const int newy)
  {
    x = newx;
    y = newy;
  }
  point(const point & other)
  {
    x = other.x;
    y = other.y;
  }
  point(const floatpoint & other);
  point & operator=(const point & other)
  {
    x = other.x;
    y = other.y;
    return *this;
  }
  point & operator=(const floatpoint & other);
  int x;
  int y;

};

struct floatpoint
{
  floatpoint()
  {
  }
  floatpoint(const double newx, const double newy)
  {
    x = newx;
    y = newy;
  }
  floatpoint(const point & other)
  {
    x = double(other.x);
    y = double(other.y);
  }
  floatpoint(const floatpoint & other)
  {
    x = other.x;
    y = other.y;
  }
  floatpoint & operator=(const floatpoint & other)
  {
    x = other.x;
    y = other.y;
    return *this;
  }
  floatpoint & operator=(const point & other)
  {
    x = double(other.x);
    y = double(other.y);
    return *this;
  }

  floatpoint operator-(const floatpoint & other) const
  {
    return floatpoint(x - other.x, y - other.y);
  }
  floatpoint operator+(const floatpoint & other) const
  {
    return floatpoint(x + other.x, y + other.y);
  }
  floatpoint operator*(const double other) const
  {
    return floatpoint(x * other, y * other);
  }
  floatpoint operator/(const double other) const
  {
    return floatpoint(x / other, y / other);
  }

  double x;
  double y;

  double magval()
  {
    return sqrt(x * x + y * y);
  }
  double angval()
  {
    return atan2(y,x);
  }
  void polar(double mag, double ang)
  {
    x = mag * cos(ang);
    y = mag * sin(ang);
  }
};

inline point & point::operator=(const floatpoint & other)
{
  x = int(other.x);
  y = int(other.y);
  return *this;
}

inline point::point(const floatpoint & other)
{
  x = int(other.x);
  y = int(other.y);
}

#endif
